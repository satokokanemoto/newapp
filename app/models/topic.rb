class Topic < ApplicationRecord
  has_many :contents, dependent: :destroy
  validates :title, presence: true, length: {in: 2..100}
end
