class Content < ApplicationRecord
  belongs_to :topic, dependent: :destroy
  validates :message, presence: true, length: {in: 6..1000}
end
