# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
window.delDate = (id,title)->
  if confirm('「' + title + '」のデータ削除？')
    document.location = "/topics/delete" + id;
    return false;
