class TopicsController < ApplicationController
  # skip_before_action:verify_authenticity_token

  before_action :set_topic, only: [:edit,:create]

  layout 'topics'
   #test1
  def index
    @msg = 'index'
    # @data = Topic.all
    @page_size = 3
    # @page = (params[:page] || 1).to_i - 1

    @page = params[:page].nil? ? 0 : params[:page].to_i - 1 # #page = 1に
    @topics_count = Topic.where('id > 0')
    @topics = Topic.order('id ASC').offset(@page_size * @page).limit(@page_size)

    # @data = Topic.order(:id).limit(page_size).offset(page_size*1)
    # @page_num = page_num
    # render topics/index
  end

  def show
    @topic = Topic.find(params[:id])
    @contents = @topic.contents.order('created_at desc')
    @content = Content.new(topic_id: @topic.id)
    # 新たな箱を作る
    # @content = Content.order("message.created_at desc").new(topic_id: @topic.id)
    if request.post?

      render 'topics/show'
      if @contents.save

        　　　　　
        @contents = @topic.contents.order('created_at desc')
      else
        @msg = '文字を記入してください'

      end

    else
      @content = Content.new(topic_id: @topic.id)
    end
      end

  def page
    page_size = 3
    # page_num = parmas[:id] == nil ?0 :params[:id].to_i - 1
    # @topics = Topic.order(published: :desc).limit(page_size).offset(page_size*page_num)
    @page = params[:id].to_i
    # @topics_count = Topic.where("id > 0")
    @topics = Topic.order(published: :desc).limit(page_size).offset(page_size * page_num)
    # @count = @topics.count
    # カウントされていないっぽい
    render topics / index
  end

  def create
    @text = 'create'
    @topic = Topic.new(topic_params)
    if request.post?
      @topic = Topic.create(topic_params)

      if @topic.save
        　　　
        goback
      else
        @msg = '文字を記入してください'
        render 'create'
      end

    else
      @topic = Topic.new
    end
  end

  def edit
    @msg = 'edit'
    # @topic = Topic.find(params[:id])
    # @topic = Topic.find(@mytopics)
    if request.patch?
      @topic.update(topic_params)
      goback
   end
  end

  def destroy
    obj = Topic.find(params[:id])
    obj.destroy
    redirect_to '/topics'
  end

  def find
    # @topics = Topic.find(params[:id])

    @topics = []
    @topics = Topic.where title: params[:find] if request.post?
  end

  # begin_actionの追加

  private

  def set_topic
    @topic = Topic.find(params[:id])
  end

  def topic_params
    params.require(:topic).permit(:title)
  end

  def goback
    redirect_to '/topics'
  end
end

#    def index
#       @msg = 'Topic data'
#
#       if request.post? then
#         obj = Topic.create(
#           title: params['title']
#         )
#         #obj.save
#       end
#       @data = Topic.all
#     end
# end
# =begin
#   def initialize
#      super
#      begin
#        @topics_data = JSON.parse(File.read("data.txt"))
#      rescue
#        @topics_data = Hash.new
#     end
#     @topics_data.each do |key,obj|
#       if Time.now.to_i - key.to_i > 24*60*60 then
#            @topics_data.delete(key)
#       end
#     end
#     File.write("data.txt", @topics_data.to_json)
#   end
#
#
#   def index
#       if request.post? then
#           obj = MyData.new(title:params['title'])
#           @topics_data[Time.now.to_i] = obj
#           data = @topics_data.to_json
#           File.write("data.txt", data)
#           @topics_data = JSON.parse(data)
#        end
#   end
# end
#
# class MyData
#    attr_accessor :title
#    ##attr_accessor :msg
#
#    def initialize  title:title
#      self.title = title
#
#    end
#
# end
