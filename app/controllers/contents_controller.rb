class ContentsController < ApplicationController
  def index
    @msg = 'index'
    @data = Content.all
  end

  def create
    #     #@msg = 'content#add'
    #     if request.post? then
    #        Content.create(content_params)
    #        goback
    #     else
    #        @content = Content.new
    #
    #     end
    #   end

    if request.post?

      @content = Content.create(content_params)

      if @content.save

      else
      @msg = '文字を記入してください'

      # render 'add'
      @topic = @content.topic
      @contents = @topic.contents.order('created_at desc')
      render template: 'topics/show'

      end

    else
      @content = Content.new
    end
end

  # 別のコントローラーのViewないでバリデーション表示
  # def add
  #   @text = 'add'
  #      #@topic = Topic.new topic_params
  #   if request.post? then
  #       @topic = Topic.create(topic_params)
  #
  #       if @topic.save then
  #
  #       goback
  #       else
  #       @msg = "文字を記入してください"
  #       render 'add'
  #       end
  #
  #
  #   else
  #   @topic = Topic.new
  #   end
  # end

  def edit
    @msg = 'edit'
    @content = Content.find(params[:id])
    if request.patch?
      @content.update(content_params)
      goback
    end
  end

  def delete
    Content.find(params[:id]).destroy
    goback
  end

  private

  def content_params
    params.require(:content).permit(:topic_id, :message)
  end

  private

  def goback
    redirect_to '/contents'
  end
end
