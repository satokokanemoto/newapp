class CreateContents < ActiveRecord::Migration[5.1]
  def change
    create_table :contents do |t|
      t.integer :topic_id
      t.string :name
      t.text :message

      t.timestamps
    end
  end
end
