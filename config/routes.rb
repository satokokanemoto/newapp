Rails.application.routes.draw do

  root 'topics#index'
  resources :topics
  resources :contents

=begin
  get 'topics', to: 'topics#index'
  get 'topics/index'
  post 'topics/index'

  get 'topics/show/:id', to: 'topics#show'

  get 'topics/add'
  post 'topics/add'

  get 'topics/:id/edit', to: 'topics#edit'
  patch 'topics/edit/:id', to: 'topics#edit'

  get 'topics/delete/:id', to: 'topics#delete'

  get 'topics/find'
  post 'topics/find'

  get 'contents/index'
  get 'contents', to: 'contents#index'
  get	'contents/index/:id', to: 'contents#index'

  get 'contents/add'
  post 'contents/add'
  patch 'contents/add'

  get 'contents/edit'
  get 'contents/edit/:id', to: 'contents#edit'
  patch 'contents/edit/:id', to: 'contents#edit'

  get 'contents/delete/:id', to: 'contents#delete'
=end
  # post 'topics', to: 'topics#index'
  # post 'topics/index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
